BY GREG HARVEY
http://www.drupaler.co.uk

FILE FIELD UPLOAD LIMIT

This module was created to allow us to have many different upload fields, using the File Field CCK module, and for each one to have it's own maximum file size, just like each can already have it's own file extension. We needed this because we wanted to facilitate MP3 uploads, but we didn't want to permit 10MB text files!

This module stores the settings for each CCK file field in an array. It uses hook_filefield (and partially documents that hook too!) to extend the validation of filefield.module, preventing files which are too large from being saved.

INSTALLATION

Install the module in the usual way - the only dependency is filefield (obviously). After installation you will note a new, additional, "Maximum file size" field when editing or creating a file field in CCK.